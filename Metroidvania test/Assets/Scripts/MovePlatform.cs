﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour {

    [SerializeField] private float _speed;
   public Vector2 position2;
    public bool hasCollided;

    // Moves palrfrom to position 2
    private void FixedUpdate() {
        if(hasCollided) {

            float step = _speed * Time.deltaTime;
            transform.position = Vector2.MoveTowards(transform.position, position2, step);
        }
    }

    // Sets bool to true on player trigger enter
    public void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            hasCollided = true;
        }
    }
   
    public float PlatformSpeed
    {
        get { return _speed; }
        set { _speed = value; }
    }
}





