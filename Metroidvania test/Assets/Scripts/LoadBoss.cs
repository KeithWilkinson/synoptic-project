﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadBoss : MonoBehaviour {

    public GameObject flashEffect;
    public Animator flashAnimator;
    public GameObject area4;
    public GameObject area5;

    public GameObject healthPickups;
    public GameObject enemies;

    public GameObject bossEdgeCollider;
   
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        

	}

    // Plays flash animation and activates and deactivates areas
    private void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            flashEffect.SetActive(true);
            flashAnimator.SetBool("startBossBattle", true);
            area5.SetActive(true);
            area4.SetActive(false);
            healthPickups.SetActive(false);
            enemies.SetActive(false);
            bossEdgeCollider.SetActive(true);
           
        }

    }
}
