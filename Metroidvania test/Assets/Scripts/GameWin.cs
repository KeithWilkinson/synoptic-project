﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameWin : MonoBehaviour {

    public GameObject boss;
    public ParticleSystem[] confetti;
    public Animator winScreenAnimation;

    public GameObject ammoUi;
    public GameObject healthText;

	// Use this for initialization
	void Start () {

       
	}
	
	// checks if boss is null and starts coroutine
	void Update () {

        if(boss == null) {

            StartCoroutine("WinEffect");
            ammoUi.SetActive(false);
            healthText.SetActive(false);
        }
		
	}

    IEnumerator WinEffect() {

        for(int i = 0; i < confetti.Length; i++) {

            confetti[i].gameObject.SetActive(true);
        }

        yield return new WaitForSeconds(5f);

        for (int i = 0; i < confetti.Length; i++) {

            confetti[i].gameObject.SetActive(false);
        }

        winScreenAnimation.SetBool("isGameWon", true);

        yield return new WaitForSeconds(3f);

        Time.timeScale = 0;
    }
}
