﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundEnemyController : MonoBehaviour {

    public float speed;
    public bool facingRight = true;
    [SerializeField] private Vector2 position1;
    [SerializeField] private Vector2 position2;
  
	// Moves the enemy object to the positions
	void Update () {

        transform.position = Vector2.Lerp(position1, position2, Mathf.PingPong(Time.time * speed, 1.0f));
       
	}
}
