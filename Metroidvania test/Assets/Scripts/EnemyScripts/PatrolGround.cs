﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolGround : MonoBehaviour {

    [SerializeField] private float _moveSpeed;

    Rigidbody2D rb;

    // Makes reference to components
	void Start () {

        rb = GetComponent<Rigidbody2D>();
	}
	
	// Flips enemy sprite
	void Update () {

        if (isFacingright()) {

            rb.velocity = new Vector2(_moveSpeed, 0f);
        }
        else {

            rb.velocity = new Vector2(-_moveSpeed, 0f);
        }
	}

    private bool isFacingright() {

        return transform.localScale.x > Mathf.Epsilon;
    }

    // Check if collider has left trigger zone
    private void OnTriggerExit2D(Collider2D collision) {

        transform.localScale = new Vector2(-Mathf.Sign(rb.velocity.x), transform.localScale.y);

    }

    public float EnemySpeed
    {
        get { return _moveSpeed; }
        set { _moveSpeed = value; }
    }
}
