﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    [SerializeField]  int _enemyHealth;
    [SerializeField] private int _enemyDamage;
    public GameObject enemy;

    public AudioSource enemyDeath;

    // Damages and destroys enemy
    public void TakeDamage(int damage) {

        _enemyHealth -= damage;
    
        if (_enemyHealth <= 0) {

            enemyDeath.Play();
            Destroy(enemy);
        } 
    }

    public int EnemyHealth
    {
        get { return _enemyHealth; }
        set { _enemyHealth = value; }
    } 

    public int EnemyDamage
    {
        get { return _enemyDamage; }
        set { _enemyDamage = value; }
    }
}
