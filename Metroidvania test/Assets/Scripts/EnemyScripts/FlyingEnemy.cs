﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyingEnemy : MonoBehaviour {

    public float speed, circleRadius;

    private Rigidbody2D enemyRb;
    public GameObject rightCheck, roofCheck, groundCheck;
    public LayerMask groundLayer;
    private bool facingRight = true, groundTouch, roofTouch, righttouch;
    public float dirX = 1, dirY = 0.25f;

    // Makes reference to attached rigidbody
    void Start() {

        enemyRb = GetComponent<Rigidbody2D>();
    }

    // Move the enemy and calls the hit detection function
    void FixedUpdate() {

        enemyRb.velocity = new Vector2(dirX, dirY) * speed * Time.fixedDeltaTime;
        HitDetection();
    }
    
    // Creates overlap circlue at each check point the size of the radius variable and calls direction 
    void HitDetection()
    {
        righttouch = Physics2D.OverlapCircle(rightCheck.transform.position, circleRadius, groundLayer);
        roofTouch = Physics2D.OverlapCircle(roofCheck.transform.position, circleRadius, groundLayer);
        groundTouch = Physics2D.OverlapCircle(groundCheck.transform.position, circleRadius, groundLayer);
        DirectionCollision();
    }

    // Flips spirte based on collision
    void DirectionCollision()
    {
        if (righttouch && facingRight) {

            Flip();
        }
        else if (righttouch && !facingRight) {

            Flip();
        }
        if (roofTouch) {

            dirY = -0.25f;
        }
        else if (groundTouch) {

            dirY = 0.25f;
        }
    }

    // Flips spirte
    void Flip() {

        facingRight = !facingRight;
        transform.Rotate(new Vector3(0, 180, 0));
        dirX = -dirX;
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(rightCheck.transform.position, circleRadius);
        Gizmos.DrawWireSphere(roofCheck.transform.position, circleRadius);
        Gizmos.DrawWireSphere(groundCheck.transform.position, circleRadius);
    }
}
