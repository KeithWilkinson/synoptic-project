﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour {

    public GameObject cam1;
    public GameObject cam2;
    public GameObject boss;

    public GameObject bossBackground;

    public GameObject bossHealthBar;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Sets cameras and boss game object
    private void OnTriggerEnter2D(Collider2D collision) {

        cam1.SetActive(false);
        cam2.SetActive(true);
        boss.SetActive(true);
        bossBackground.SetActive(true);
        bossHealthBar.SetActive(true);
    }
}
