﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadBossEnvironment : MonoBehaviour {

    public GameObject bossBackground;
    public GameObject[] bossAreaAssets;
    public GameObject iceCaveBackground;
    public AudioSource icecaveOst;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Activates game objects
    public void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            bossBackground.SetActive(true);

            for(int i = 0; i < bossAreaAssets.Length; i++) {

                bossAreaAssets[i].SetActive(true);
            }

            Destroy(iceCaveBackground);
            icecaveOst.Stop();
        }

    }   
}