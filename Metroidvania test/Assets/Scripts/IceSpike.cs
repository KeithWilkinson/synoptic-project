﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IceSpike : MonoBehaviour {

    private Rigidbody2D _rb;
    [SerializeField] private bool _isFalling = false;

	// Makes reference to rigidbody
	void Start () {

        _rb = GetComponent<Rigidbody2D>();
	}

    // Increases downwards move speed
    private void FixedUpdate() {

        if(IsFalling == true) {

            _rb.AddForce(Vector3.down * 5);
        }
    }
    
    // sets bool and rigidbody on player collision
    private void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            _rb.isKinematic = false;
            _isFalling = true;

        }
    }

    public bool IsFalling
    {
        get { return _isFalling; }
        set { _isFalling = value; }
    }
}
