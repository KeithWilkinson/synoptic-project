﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrippleJumpUnlock : MonoBehaviour {

    public PlayerController playerControl;
    public GameObject triplePickup;
    
    // Sets bool to true and destroys object on player collision
    public void OnCollisionEnter2D(Collision2D collision) {

        if(collision.gameObject.tag == "Player") {

            playerControl.cantripleJump = true;
            Destroy(triplePickup);
        }
    }
}
