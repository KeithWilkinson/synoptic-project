﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ScreenShake : MonoBehaviour {

    public static ScreenShake Instance { get; private set; }
    private CinemachineVirtualCamera _cinemachineCamera;
    private float _shakeTimer;

    // Gets cinemachine component
    private void Awake() {

        Instance = this;
       _cinemachineCamera = GetComponent<CinemachineVirtualCamera>();
    }

    // Shakes camera
    public void ShakeCamera(float intensity, float time) {

        CinemachineBasicMultiChannelPerlin CinemachineBasicMultiChannelPerlin = 
        _cinemachineCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        CinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensity;
        _shakeTimer = time;
    }

    // Stops camera shake based on time
    private void Update() {

        if (_shakeTimer > 0) {


            _shakeTimer -= Time.deltaTime;

            if (_shakeTimer <= 0f) {

                //Time over
                CinemachineBasicMultiChannelPerlin CinemachineBasicMultiChannelPerlin =
              _cinemachineCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                CinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0;
            }
         
        }
       
    }

}
