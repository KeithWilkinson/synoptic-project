﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJump : MonoBehaviour {

    public float fallMultiplier;
    public float lowjumpMultiplier;

    private Rigidbody2D _playerRigidbody;


	// Gets Rigidbody2D attached to player
	void Start () {

        _playerRigidbody = GetComponent<Rigidbody2D>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // Checks player's Rigidbody Y velocity value
    void FixedUpdate() {

        if(_playerRigidbody.velocity.y < 0) {

            _playerRigidbody.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        } else if(_playerRigidbody.velocity.y > 0 && !Input.GetButton("Jump")) {

            _playerRigidbody.velocity += Vector2.up * Physics2D.gravity.y * (lowjumpMultiplier - 1) * Time.deltaTime;
        }
    }
}
