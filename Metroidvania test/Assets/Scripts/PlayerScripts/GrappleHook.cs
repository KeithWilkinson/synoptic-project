﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MonoBehaviour
{

    private Vector2 _mousePos;
    private Vector2 _clickPos;
    private Camera _camera;

    private static bool isGrappleing;

    private LineRenderer _lineRenderer;

    public LayerMask grappleLayer;
    // Makes reference to components 
    void Start() {

        _camera = Camera.main;
        _lineRenderer = GetComponent<LineRenderer>();
        isGrappleing = true;
        _lineRenderer.positionCount = 0;
    }
    
    // Calls drawline function based on player input
    void Update() {

        GetMousePos();
        if (Input.GetMouseButtonDown(1) && isGrappleing) {

            _lineRenderer.positionCount = 2;
            _clickPos = _mousePos;
            isGrappleing = false;
        }
        else if (Input.GetMouseButtonUp(1)) {

            isGrappleing = true;
            _lineRenderer.positionCount = 0;
        }
        DrawLine();
    }

    // Draws line from set point to mouse click point
    void DrawLine() {

        if (_lineRenderer.positionCount <= 0) return;
        _lineRenderer.SetPosition(0, transform.position);
        _lineRenderer.SetPosition(1, _clickPos);
    }
    
    // Get mouse position in world space
    void GetMousePos() {

        _mousePos = _camera.ScreenToWorldPoint(Input.mousePosition);
    }

    public static bool Grappled
    {
        get { return isGrappleing; }
    }
}
