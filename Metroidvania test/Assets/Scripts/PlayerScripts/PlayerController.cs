﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float jumpForce;
    public float moveInput;
    public float checkRadius;
    public int extraJumps;
    public Transform groundCheck;
    public LayerMask ground;
    private Rigidbody2D rb;
    public bool isGrounded;

    bool facingRight = true;
    private Vector2 grapplePos;
    public float pullSpeed = 4f;

    public bool cantripleJump = false;
  
	// Makes reference to the rigidbody component
	void Start() {

        rb = GetComponent<Rigidbody2D>();
    }
		
	// Allows the player to double or triple jump
	void Update () {

        if(isGrounded == true) {
            
            if(cantripleJump == true) {

                extraJumps = 2;
            }
            else {

                extraJumps = 1;
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && extraJumps > 0) {
            rb.velocity = Vector2.up * jumpForce;
            extraJumps--;
        }

	}

    // Moves the player left and right 
    void FixedUpdate() {

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, ground);

        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        if(moveInput < 0 && facingRight) {

            Flip();
        }
        else if(moveInput > 0 && !facingRight) {

            Flip();
        }

        if (GrappleHook.Grappled) {

            rb.gravityScale = 1;
            speed = 8f;
        }
        else {

            rb.gravityScale = 0;
            speed = 0f;
            grapplePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = Vector2.MoveTowards(transform.position, grapplePos, pullSpeed * Time.deltaTime);
        }
    }

    // Flips the player sprite
    void Flip() {

        facingRight = !facingRight;
        transform.Rotate(0f, 180f, 0f);
    }
}
