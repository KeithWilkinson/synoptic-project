﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerHealth : MonoBehaviour {

    [SerializeField] private int _playerHealth;
    private int _playerCurrentHealth;
    public Rigidbody2D rb;
    public float knockback;

    public GameObject playerHealthText;
    public GameObject bulletUI;
    public TMP_Text healthtext;

    public Animator gameOverAnimator;
    public GameObject gameOverScreen;

    [SerializeField] private PlayerController _playerMovement;

    public GameObject healthPickupEffect;

	// Makes reference to rigidbody and player health text
	void Start () {

        rb = GetComponent<Rigidbody2D>();
        healthtext.text = _playerHealth.ToString("0");
        _playerMovement = GetComponent<PlayerController>();
      
	}

    // Ends game if player's health is les than or equal to 0
    void Update() {

        if(_playerHealth <= 0) {

            playerHealthText.SetActive(false);
            bulletUI.SetActive(false);
            gameOverScreen.SetActive(true);
            gameOverAnimator.SetBool("isGameOver", true);
            StartCoroutine("StopTime");
            
        }
    }

    IEnumerator StopTime() {

        yield return new WaitForSeconds(3f);
        Time.timeScale = 0;
    }
    
    // checks if the player has collided with an enemy
    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy") {

            DamagePlayer();
            rb.AddForce(new Vector2(0, knockback), ForceMode2D.Impulse);
            StartCoroutine("PlayerDamage");
        }

        if(collision.gameObject.tag == "Hazard") {

            DamagePlayer();
            rb.AddForce(new Vector2(0, knockback), ForceMode2D.Impulse);
            StartCoroutine("PlayerDamage");
        }
    }

    public void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "HealthPickup") {

           if(_playerHealth >= 20) {

                return;

           }
           else {

                _playerHealth += 1;
                healthtext.text = _playerHealth.ToString("0");
                Destroy(collision.gameObject);
                Instantiate(healthPickupEffect, transform.position, Quaternion.identity);
           }
        }
    }
    
    IEnumerator PlayerDamage(){

        _playerMovement.enabled = false;
        yield return new WaitForSeconds(1f);
        _playerMovement.enabled = true;
    }

    // Damages player 
    public void DamagePlayer() {

        _playerHealth -= 1;
        healthtext.text = _playerHealth.ToString("0");
    }
    
    public int PlayersHealth
    {
        get { return _playerHealth; }
        set { _playerHealth = value; }
    }
}

