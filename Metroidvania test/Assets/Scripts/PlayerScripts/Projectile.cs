﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float bulletSpeed;
    public float lifeTime;
    public float rayDistance;
    public LayerMask whatIsSolid;
    [SerializeField]private int _damage;

    public GameObject bulletExplosion;

	// Use this for initialization
	void Start () {

        Invoke("DestroyProjectile", lifeTime);
	}
	
	// Update is called once per frame
	void Update () {

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.right, rayDistance, whatIsSolid);
        if(hitInfo.collider != null) {
            if (hitInfo.collider.CompareTag("Enemy")) {
                Debug.Log("it hit");
                hitInfo.collider.GetComponent<Enemy>().TakeDamage(_damage);
                Instantiate(bulletExplosion, transform.position, Quaternion.identity);
            }
            if (hitInfo.collider.CompareTag("Miniboss")) {
                Debug.Log("boom");
                hitInfo.collider.GetComponent<Miniboss>().TakeDamage(_damage);
                Instantiate(bulletExplosion, transform.position, Quaternion.identity);
            }
            if (hitInfo.collider.CompareTag("Boss")) {

                hitInfo.collider.GetComponent<BossHealth>().DamageBoss(_damage);
                Instantiate(bulletExplosion, transform.position, Quaternion.identity);
            }
            DestroyProjectile();
        }

        transform.Translate(Vector2.right * bulletSpeed * Time.deltaTime);
	}

    void DestroyProjectile() {
        Destroy(gameObject);
    }

    public int Damage
    {
        get { return _damage; }
        set { _damage = value; }
    }
}
