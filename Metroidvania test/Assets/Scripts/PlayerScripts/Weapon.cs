﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Weapon : MonoBehaviour {

    public float offset;
    private float shotCooldown;
    public float startShotCooldown;

    public GameObject projectile;
    public GameObject projectile1, projectile2;
    public Transform shotPoint;

    public GameObject projectile1Ui;
    public GameObject projectile2Ui;

    public bool projectile2PickedUp = false;
    public GameObject projectile2Pickup;

    public GameObject[] greenWalls;

    private AudioSource _blasterEffect;

    public bool muzzleFlashEnabled = false;
    public float muzzleflashTimer = 0.1f;
    public float muzzleTimerStart;
    public GameObject muzzleEffect;
    


	// Sets default projectile
	void Start () {

        _blasterEffect = GetComponent<AudioSource>();
        projectile = projectile1;
        projectile1Ui.SetActive(true);
        muzzleTimerStart = muzzleflashTimer;
	}
	
	// Calls shoot function based on player input and reduces shotcooldown variable
	void Update () {

        if(muzzleFlashEnabled == true) {

            muzzleEffect.SetActive(true);
            muzzleflashTimer -= Time.deltaTime;
        }

        if(muzzleflashTimer <= 0) {

            muzzleFlashEnabled = false;
            muzzleEffect.SetActive(false);
            muzzleflashTimer = muzzleTimerStart;
        }

        SwitchBullet();

        if(shotCooldown <= 0) {

            if (Input.GetMouseButtonDown(0)) {

                muzzleFlashEnabled = true;
                muzzleEffect.SetActive(true);
                Shoot();
                shotCooldown = startShotCooldown;
            }

        }
        else {
            shotCooldown -= Time.deltaTime;
        }
	}

    // checks to see if player has collided with tagged game object 
    void OnCollisionEnter2D(Collision2D collision) {

        if(collision.gameObject.tag == "Projectile2") {

            Debug.Log("yeah");
            Destroy(projectile2Pickup);
            projectile2PickedUp = true;
            for (int i = 0; i < greenWalls.Length; i++)
            {

                Destroy(greenWalls[i].gameObject);
            }
        }
    }
    
    // Shoots projectile and calls screen shake function
    void Shoot() {

        _blasterEffect.Play();
        Instantiate(projectile, shotPoint.position, shotPoint.rotation);
        ScreenShake.Instance.ShakeCamera(5f, .1f);
    }

    // Switches projectile type and UI object
    void SwitchBullet() {

        if (Input.GetKeyDown(KeyCode.Alpha1)) {

            projectile = projectile1;
            projectile1Ui.SetActive(true);
            projectile2Ui.SetActive(false);
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha2) && projectile2PickedUp == true) {

            projectile = projectile2;
            projectile2Ui.SetActive(true);
            projectile1Ui.SetActive(false);
        }
    }

}
