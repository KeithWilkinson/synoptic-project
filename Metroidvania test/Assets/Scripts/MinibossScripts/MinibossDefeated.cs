﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinibossDefeated : MonoBehaviour {

    public GameObject blaze;
    public GameObject frost;
    public GameObject level2Lift;
    public GameObject blazeHealthBar;
    public GameObject frostHealthBar;

    public GameObject area1;

    public GameObject caveAreaEnterTrigger;
    public GameObject forestAreaTrigger;

    public AudioSource forestBgm;
    public AudioSource minibossBgm;
    public GameObject resumeForestBgm;

    public GameObject caveBarricade;

    public bool isminiBossDefeated = false;
    
    
	// Use this for initialization
	void Start () {
		
	}
	
	// Destroys lift if both objects are null
	void Update () {
		
        if(blaze == null && frost == null) {

            resumeForestBgm.SetActive(true);
            minibossBgm.Stop();
            Destroy(level2Lift);
            area1.SetActive(true);
            caveAreaEnterTrigger.SetActive(true);
            forestAreaTrigger.SetActive(false);
            caveBarricade.SetActive(false);
        }

	}

    // Activates game objects and UI elements when player enters trigger zone
    private void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            forestBgm.Stop();
            minibossBgm.Play();
            blaze.SetActive(true);
            frost.SetActive(true);
            blazeHealthBar.SetActive(true);
            frostHealthBar.SetActive(true);
        }
    }
}
