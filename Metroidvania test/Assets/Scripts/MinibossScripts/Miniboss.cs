﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Miniboss : MonoBehaviour {

    [SerializeField] private float _speed;
    [SerializeField] private float _lineOfSite;
    [SerializeField] private float _shootRange;
    [SerializeField]private float _fireRate = 1f;
    [SerializeField] private float _nextFireTime;
    
    [SerializeField] private GameObject _bullet;
    [SerializeField] private GameObject _bulletShotpoint;
    private Transform _playerTransform;

    private bool facingRight = false;

    public int minibossHealth;
    public GameObject healthBar;

	// Finds player's transform
	void Start () {

        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	// Moves enemy if player enters line of sight and compares transform to player
	void Update () {

        float distanceFromPlayer = Vector2.Distance(_playerTransform.position, transform.position);
        if(distanceFromPlayer < _lineOfSite && distanceFromPlayer > _shootRange) {

            transform.position = Vector2.MoveTowards(this.transform.position, _playerTransform.position, _speed * Time.deltaTime);
        }
        else if (distanceFromPlayer <= _shootRange && _nextFireTime < Time.time) {

            Instantiate(_bullet, _bulletShotpoint.transform.position, Quaternion.identity);
            _nextFireTime = Time.time + _fireRate;
        }

        if(_playerTransform.transform.position.x > gameObject.transform.position.x && facingRight) {

            Flip();
        }
        if(_playerTransform.transform.position.x < gameObject.transform.position.x && !facingRight) {

            Flip();
        }

	}

    // Flips spirte
    void Flip() {

        facingRight = !facingRight;
        Vector3 tmpScale = gameObject.transform.localScale;
        tmpScale.x *= -1;
        gameObject.transform.localScale = tmpScale;
    }

     public void TakeDamage(int damage) {

        minibossHealth -= damage;
    
        if ( minibossHealth <= 0) {

            Destroy(gameObject);
            Destroy(healthBar);
        } 
     }

    private void OnDrawGizmosSelected() {

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _lineOfSite);
        Gizmos.DrawWireSphere(transform.position, _shootRange);
    }

    public float MinibossSpeed
    {
        get { return _speed; }
        set { _speed = value; }
    }

    public float MinibossLineofSight
    {
        get { return _lineOfSite; }
        set { _lineOfSite = value; }
    }

    public float MinibossShootRange
    {
        get { return _shootRange; }
        set { _shootRange = value; }
    }

    public float MinibossfireRate
    {
        get { return _fireRate; }
        set { _fireRate = value; }
    }

    public GameObject MinibossBullet
    {
        get { return _bullet; }
        set { _bullet = value; }
    }

    public GameObject MbShotPoint
    {
        get { return _bulletShotpoint; }
        set { _bullet = value; }
    }

}
