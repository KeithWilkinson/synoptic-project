﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinibossSeparation : MonoBehaviour {

    public GameObject[] miniBoss;
    [SerializeField] private float _spaceBetween;

	// Fills miniboss game object array
	void Start () {

        miniBoss = GameObject.FindGameObjectsWithTag("Miniboss");
	}
	
	// Prevents miniboss game objects from over lapping
	void Update () {
		
        foreach(GameObject go in miniBoss) {

            if(go != gameObject) {

                float distance = Vector3.Distance(go.transform.position, this.transform.position);
                if(distance <= _spaceBetween) {

                    Vector3 direction = transform.position - go.transform.position;
                    transform.Translate(direction * Time.deltaTime);
                }
            }
        }
	}

    public float SpaceBetweenMiniboss
    {
        get { return _spaceBetween; }
        set { _spaceBetween = value; }
    }
}
