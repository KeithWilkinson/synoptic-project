﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingProjectile : MonoBehaviour {

    [SerializeField] private float _projectileSpeed;
    [SerializeField] private Transform _playerTransform;
    [SerializeField] private float _projectileLifetime;

	// Finds player transform and involkes
	void Start () {

        Invoke("DestroyBullet",_projectileLifetime);
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
		
	}
	
	// Moves bullet towards player
	void Update () {

        transform.position = Vector2.MoveTowards(transform.position, _playerTransform.position, _projectileSpeed * Time.deltaTime);

	}

    // Destroys bullet based on collision
    private void OnCollisionEnter2D(Collision2D collision) {

        if(collision.gameObject.tag == "Player") {

            Destroy(gameObject);
        }

        if(collision.gameObject.layer == 8) {

            Destroy(gameObject);
        }
    }
    
    // Destroys game object
    void DestroyBullet() {

        Destroy(gameObject);
    }
}