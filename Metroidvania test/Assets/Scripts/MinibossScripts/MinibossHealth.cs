﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinibossHealth : MonoBehaviour {

    public Image healthBar;
    public float currentHealth;
    [SerializeField] private float _maxHealth;
    public Miniboss miniBoss;

	// Use this for initialization
	void Start () {
		
	}
	
	// Sets current healthbar equal to current health
	void Update () {

        currentHealth = miniBoss.minibossHealth;
        healthBar.fillAmount = currentHealth / _maxHealth;

    }

    public float MiniBossHealth
    {
        get { return _maxHealth; }
        set { _maxHealth = value; }
    }
}
