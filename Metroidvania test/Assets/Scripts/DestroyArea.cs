﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyArea : MonoBehaviour {

    public GameObject[] areaToDestroy;

    // Destroys area if player enters trigger
    public void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            for(int i = 0; i < areaToDestroy.Length; i++) {

                areaToDestroy[i].SetActive(false);
            }
        }
    }
}