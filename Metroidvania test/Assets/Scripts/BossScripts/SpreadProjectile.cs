﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadProjectile : MonoBehaviour {

    public float bulletSpeed;
    private Rigidbody2D rb;

	// Gets rigidbody component
	void Start () {

        rb = GetComponent<Rigidbody2D>();
    }

    // Destroys game object on collision
    private void OnCollisionEnter2D(Collision2D collision) {

        if(collision.gameObject.tag == "Player") {

            Destroy(gameObject);
        }
    }
    
    // Moves bullet to the left and destroys object after X time has passed
    private void FixedUpdate() {

        rb.velocity = -transform.right * bulletSpeed;
        Destroy(gameObject, 5);
    }
}
