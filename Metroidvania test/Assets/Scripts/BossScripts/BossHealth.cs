﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BossHealth : MonoBehaviour {

    public int bossMaxHealth;
    public Image bossHealthBar;
    [SerializeField] private int _maxHealth;
    public BossBattle bossBattle;
    public float currentBossHealth;

    public GameObject healthBar;

	// Use this for initialization
	void Start () {
		
	}
	
	// Sets image fill amount equal to boss health
	void Update () {

        currentBossHealth = bossBattle._bossHealth;
        bossHealthBar.fillAmount = currentBossHealth / _maxHealth;
		
	}


    // Destroys game object if health is less than or equal to 0
    public void DamageBoss(int damage) {

        bossMaxHealth -= damage;

        if(bossMaxHealth <= 0) {

            Destroy(gameObject);
            Destroy(healthBar);
        }
    }
}
