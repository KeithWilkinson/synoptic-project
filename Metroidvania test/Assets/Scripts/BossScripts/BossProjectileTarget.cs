﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossProjectileTarget : MonoBehaviour {


    [SerializeField] private float _targetChangeRate;
    [SerializeField] private float _nextChangetime;

    public List<Vector3> targetPositions = new List<Vector3>();

    // calls function
    void Start() {

        MoveTarget();
    }

    // Calls function based on time
    private void Update() {

        if(_nextChangetime < Time.time) {

            MoveTarget();
        }
    }
    
    // Moves game object and resets timer
    public void MoveTarget() {

        transform.position = targetPositions[Random.Range(0, targetPositions.Count)];
        _nextChangetime = Time.time + _targetChangeRate;
    }

    public float TargetchangeRate
    {
        get { return _targetChangeRate; }
        set { _targetChangeRate = value; }
    }

    public float NextChangeRate
    {
        get { return _nextChangetime; }
        set { _nextChangetime = value; }
    }
}
