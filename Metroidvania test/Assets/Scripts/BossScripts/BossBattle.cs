﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBattle : MonoBehaviour {

    [SerializeField] private GameObject _shotPoint;
    [SerializeField] private GameObject _sqaureProjectile;
    [SerializeField] private GameObject _burstProjectile;

    [SerializeField] private float _bossFireRate;
    [SerializeField] private float _bossNextFiretime;

    public float _bossHealth;

    public GameObject bullet1;
    public GameObject bullet2;
    public GameObject bullet3;

    public GameObject speadPoint1;
    public GameObject speadPoint2;
    public GameObject speadPoint3;

    public float bulletSpeed;

    public BossHealth bossHealth;

    public List<GameObject> rainObjects = new List<GameObject>();
    private bool _isHalfHealth = false;

    // Use this for initialization
    void Start () {

    }
	
	// Switches attack based on health
	void Update () {

        if(_bossHealth == 34) {

            _isHalfHealth = true;
        }

        _bossHealth = bossHealth.bossMaxHealth;

        if (_bossNextFiretime < Time.time) {

           if(_bossHealth > 35) {

                Fire();
           }

           if(_bossHealth < 35 && _isHalfHealth == true) {

                FireSpread();
           }

           if(_bossHealth < 15) {

                _isHalfHealth = false;
                ProjectileRain();
           }
        }
	}

    // Fires square projectile
    void Fire() {

           Instantiate(_sqaureProjectile, _shotPoint.transform.position, Quaternion.identity);
           _bossNextFiretime = Time.time + _bossFireRate;
    }

    // Fires spread projectile
    void FireSpread() {

        Instantiate(bullet1, speadPoint1.transform.position, Quaternion.identity);
        Instantiate(bullet2, speadPoint2.transform.position, Quaternion.identity);
        Instantiate(bullet3, speadPoint3.transform.position, Quaternion.identity);
        _bossNextFiretime = Time.time + _bossFireRate;

    }

    void ProjectileRain() {

        Instantiate(rainObjects[Random.Range(0, rainObjects.Count)], _shotPoint.transform.position, Quaternion.identity);
        _bossNextFiretime = Time.time + _bossFireRate;

    }

    public GameObject SquareProjectile
    {
        get { return _sqaureProjectile; }
        set { _sqaureProjectile = value; }
    }

    public GameObject BurstProjectile
    {
        get { return _burstProjectile; }
        set { _burstProjectile = value; }
    }
}
