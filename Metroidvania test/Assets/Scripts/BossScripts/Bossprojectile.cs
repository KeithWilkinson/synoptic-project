﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bossprojectile : MonoBehaviour {

    [SerializeField] private float _projectileSpeed;
    [SerializeField] private Transform _target;
 

    // Gets target game object
    void Start() {

        _target = GameObject.FindGameObjectWithTag("Target").transform;
    }
    
    // Fire projectile towards target and destroys it after a set amount of time 
    void Update() {

        transform.position = Vector2.MoveTowards(transform.position, _target.position, _projectileSpeed * Time.deltaTime);
        Destroy(gameObject, 5);
    }

    // Destroys projectile when it hits target 
    private void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Target") {

            Destroy(gameObject);
        }
    }

    public float BossProjectileSpeed
    {
        get { return _projectileSpeed; }
        set { _projectileSpeed = value; }
    }

    public Transform BossProjectileTarget
    {
        get { return _target; }
        set { _target = value; }
    }
}
