﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandardProjectile : MonoBehaviour {

    [SerializeField] private float _projectileSpeed;
    [SerializeField] private Transform _playerPos;
    [SerializeField] private float _projectileLifetime;
    private Rigidbody2D _rb2D;

	// Fires projectile towards player
	void Start () {

        _playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        _rb2D = GetComponent<Rigidbody2D>();
        Vector2 moveDir = (_playerPos.transform.position - transform.position).normalized * _projectileSpeed;
        _rb2D.velocity = new Vector2(moveDir.x, moveDir.y);
        Destroy(this.gameObject, _projectileLifetime);
       


	}
	
	// Update is called once per frame
	void Update () {

       

    }
}
