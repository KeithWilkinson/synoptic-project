﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ChangeOutfit : MonoBehaviour {

     enum BodyParts {

        Heads,
        Bodies,
        Legs,
     }

    [SerializeField] private GameObject[] _heads;
    [SerializeField] private Transform _headParent;

    [SerializeField] private GameObject[] _bodies;
    [SerializeField] private Transform _bodyParent;

    [SerializeField] private GameObject[] _legs;
    [SerializeField] private Transform _legsParent;


    GameObject activeHead;
    GameObject activeBody;
    GameObject activeLegs;

    private int _headIndex = 0;
    private int _bodyIndex = 0;
    private int _legsIndex = 0;

    public GameObject panel;
    public PlayerController charcaterController;
    public Rigidbody2D playerrigidbody2D;
    public Weapon weaponScript;
    public GameObject[] enemies;
    public GameObject[] defaultLook;
    public GameObject playerPreview;
    public GameObject bulletsUi;
    public GameObject healthUi;
    public GameObject uIYouText;

    public GameObject playerLook;

 
    // Changes to next head
    public void NextHead() {
        if (_headIndex < _heads.Length - 1)

            _headIndex++;
        else
            _headIndex = 0;

        SwitchPart(BodyParts.Heads, _headIndex);
    }

    // Changes to previous head
    public void PreviousHead() {
        if (_headIndex > 0)

            _headIndex--;

        else

          _headIndex = _heads.Length- 1;

        SwitchPart(BodyParts.Heads, _headIndex);
    }

    // Changes to next body
    public void NextBody() {
        if (_bodyIndex < _bodies.Length - 1)

            _bodyIndex++;
        else
            _bodyIndex = 0;
        SwitchPart(BodyParts.Bodies, _bodyIndex);
    }

    // Changes to previous body
    public void PreviousBody() {
        if (_bodyIndex > 0)

            _bodyIndex--;
        else
            _bodyIndex = _bodies.Length - 1;

        SwitchPart(BodyParts.Bodies, _bodyIndex);

    }

    // Changes to next legs
    public void NextLegs() {
        if (_legsIndex < _legs.Length - 1)

            _legsIndex++;
        else
            _legsIndex = 0;
        SwitchPart(BodyParts.Legs, _legsIndex);

    }

    // Changes to previous legs
    public void PreviousLegs() {
        if (_legsIndex > 0)

            _legsIndex--;
        else
            _legsIndex = _legs.Length - 1;

        SwitchPart(BodyParts.Legs, _legsIndex);

    }

    // Starts game
     public void StartGame() {

       for(int i = 0; i < enemies.Length; i++) {

            enemies[i].SetActive(true);
       }
       for(int i = 0; i < defaultLook.Length; i++) {

            defaultLook[i].SetActive(false);
       }

        Destroy(playerLook);

        panel.SetActive(false);
        charcaterController.enabled = true;
        weaponScript.enabled = true;
        playerrigidbody2D.isKinematic = false;
        bulletsUi.SetActive(true);
        healthUi.SetActive(true);
        uIYouText.SetActive(false);
        playerPreview.transform.SetParent(_headParent);
        playerPreview.transform.ResetTransform();

     }

    // Switches body parts 
    void SwitchPart(BodyParts parts, int id) {

        switch(parts) {

            case BodyParts.Heads:
                if (activeHead != null)
                    GameObject.Destroy(activeHead);

                activeHead = GameObject.Instantiate(_heads[id]);
                activeHead.transform.SetParent(_headParent);
                activeHead.transform.ResetTransform();
                break;

            case BodyParts.Bodies:
                if (activeBody != null)
                    GameObject.Destroy(activeBody);

                activeBody = GameObject.Instantiate(_bodies[id]);
                activeBody.transform.SetParent(_bodyParent);
                activeBody.transform.ResetTransform();
                break;

            case BodyParts.Legs:
                if (activeLegs != null)
                    GameObject.Destroy(activeLegs);

                activeLegs = GameObject.Instantiate(_legs[id]);
                activeLegs.transform.SetParent(_legsParent);
                activeLegs.transform.ResetTransform();
                break;
        }

    }

    public GameObject[] HeadSprites
    {
        get { return _heads; }
        set { _heads = value; }
    }

    public Transform HeadParent
    {
        get { return _headParent; }
        set { _headParent = value; }
    }

    public int HeadIndex
    {
        get { return _headIndex; }
        set { _headIndex = value; }
    }

    public GameObject[] BodySprites
    {
        get { return _bodies; }
        set { _bodies = value; }
    }

    public Transform BodyParent
    {
        get { return _bodyParent; }
        set { _bodyParent = value; }
    }

    public int BodyIndex
    {
        get { return _bodyIndex; }
        set { _bodyIndex = value; }
    }

    public GameObject[] LegSprites
    {
        get { return _legs; }
        set { _legs = value; }
    }

    public Transform LegsParent
    {
        get { return _legsParent; }
        set { _legsParent = value; }
    }

    public int LegsIndex
    {
        get { return _legsIndex; }
        set { _legsIndex = value; }
    }
}


