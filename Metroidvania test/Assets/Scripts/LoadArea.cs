﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadArea : MonoBehaviour {

    public GameObject[] areaToLoad;

    public GameObject backgroundToLoad;
    public GameObject backgroundToDestroy;

    public AudioSource musicToPlay;
    public AudioSource musicToStop;

    public GameObject barricade;

	// Makes reference to rigidbody and deactivates area 2 objects
	void Start () {

        for (int i = 0; i < areaToLoad.Length; i++) {

            areaToLoad[i].SetActive(false);
        }  
    }

    // Activates area 2 objects if player hits trigger
    public void OnTriggerEnter2D(Collider2D collision) {

        if(collision.gameObject.tag == "Player") {

            for (int i = 0; i < areaToLoad.Length; i++) {

                areaToLoad[i].SetActive(true);
               
                
            }
            barricade.SetActive(true);
            backgroundToLoad.SetActive(true);
            backgroundToDestroy.SetActive(false);
            musicToStop.Stop();
            musicToPlay.Play();
        }
    }
}
