﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour {

    // Restarts the game
    public void RestartGame() {

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Quits the game
    public void QuitGame() {

        Application.Quit();
    }
}