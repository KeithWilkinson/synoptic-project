﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lift : MonoBehaviour {

    private Rigidbody2D _rb;
    [SerializeField] private Vector2 _position1;
    [SerializeField] private Vector2 _position2;
    public float speed;

	// Use this for initialization
	void Start () {

        _rb = GetComponent<Rigidbody2D>();
	}

    private void FixedUpdate() {

        _rb.MovePosition(Vector2.Lerp(_position1, _position2, Mathf.PingPong(Time.fixedDeltaTime * speed, 1.0f)));
    }
    
        
    

    // Update is called once per frame
    void Update () {
		
	}
}
